\documentclass[]{apa6e}
\usepackage{hyperref}
\hypersetup{
    colorlinks = true,
    citecolor = {black},
    linkcolor = {blue},
    urlcolor = {blue}
}
\usepackage{apacite}
\usepackage[none]{hyphenat}
\usepackage{graphicx}
\bibliographystyle{apacite}

\abstract{The Experiences in Close Relationships Scale (ECR) is a 36-item scale designed to measure adult attachment styles. The ECR is well validated, and provides estimates of individual's attachment-related anxiety and attachment-related avoidance behaviors. This paper presents a psychometric analysis of 17,386 individual responses to the ECR, using classical test theory, exploratory factor analysis, and multidimensional item response theory. In addition to providing further support for item quality and reliability, a five-factor solution and multidimensional scoring pattern are developed.}

\begin{document}
\title{Psychometric Analysis of the Experiences in Close Relationships Scale}

\shorttitle{EXPERIENCES IN CLOSE RELATIONSHIPS}
\author{Andrew Benesh  \\ Florida State University}
\authornote{Manuscript prepared independently to meet criteria for Personality Theory.}
\date{\today} 

\maketitle

The Experiences in Close Relationships Scale \cite[ECR]{brennan1998self}, is a 36-item self-report scale designed to categorize adult attachment styles.
The based on two underlying factors and attachment-related anxiety and attachment-related avoidance.
The ECR has been widely used in adult attachment research. 
Several variants of the scale exist, including the ECR - Revised \cite{fraley2000item, sibley2004short, sibley2005reliability, fairchild2006investigating}, two shortened versions \cite{wei2007experiences, lo2009measuring}, and translations in Spanish \cite{alonso2007spanish}, Italian \cite{busonera2014psychometric}, Greek \cite{tsagarakis2007reliability}, Turkish \cite{sumer1999psychometric}, Norwegian \cite{olsson2010norwegian}, Romanian \cite{rotaru2013psychometric}, Chinese \cite{mallinckrodt2004quantitative}, Korean \cite{kim2011psychometric}, and Thai \cite{wongpakaran2011validity}. 
Versions of the ECR have also been developed for use with children and adolescents \cite{brenning2014psychometric}.

Researchers have found robust evidence for the reliability and validity of the ECR and its alternate forms, and have included evaluations using factor analysis and item response theory \cite{brennan1998self, lopez2001adult, lopez2002adult, lopez2002stability, wei2004maladaptive, vogel2005adult}.
Studies have consistently reported internal reliability for the measure between $0.89-0.95$, with test-retest reliability at 6-months between $0.68-0.71$ \cite{wei2007experiences}.

\section{Method}
\subsection{Data}
This analysis uses a secondary data set consisting of $17386$ responses to the ECR. 
Data was collected using a modified version of the ECR, in which responses are truncated from a 1--7 likert scale to a 1--5 likert scale.
The measure was administered to a convenience sample of 17,386 users of an online personality testing service. 
Data collection began in 2011 and closed in 2013.
In addition to the 36 base items, users age and sex were collected, and their nationality was inferred based on IP address.
The data was made publicly available at \href{http://personality-testing.info/_rawdata/}{personality-testing.info}.
Because this sample lacks a rigorous sampling methodology, results must be interpreted with caution.

\subsection{Instrument}
The ECR contains 36 items. Participants are asked to rate each item from 1--5, with 1 representing ``Strongly Disagree'' and 5 representing ``Strongly Agree''.
\begin{enumerate}
	\setlength\itemsep{-.5em}
\item I prefer not to show a partner how I feel deep down.
\item I worry about being abandoned.
\item I am very comfortable being close to romantic partners.
\item I worry a lot about my relationships.
\item Just when my partner starts to get close to me I find myself pulling away.
\item I worry that romantic partners wont care about me as much as I care about them.
\item I get uncomfortable when a romantic partner wants to be very close.
\item I worry a fair amount about losing my partner.
\item I don't feel comfortable opening up to romantic partners.
\item I often wish that my partner's feelings for me were as strong as my feelings for him/her.
\item I want to get close to my partner, but I keep pulling back.
\item I often want to merge completely with romantic partners, and this sometimes scares them away.
\item I am nervous when partners get too close to me.
\item I worry about being alone.
\item I feel comfortable sharing my private thoughts and feelings with my partner.
\item My desire to be very close sometimes scares people away.
\item I try to avoid getting too close to my partner.
\item I need a lot of reassurance that I am loved by my partner.
\item I find it relatively easy to get close to my partner.
\item Sometimes I feel that I force my partners to show more feeling, more commitment.
\item I find it difficult to allow myself to depend on romantic partners.
\item I do not often worry about being abandoned.
\item I prefer not to be too close to romantic partners.
\item If I can't get my partner to show interest in me, I get upset or angry.
\item I tell my partner just about everything.
\item I find that my partner(s) don't want to get as close as I would like.
\item I usually discuss my problems and concerns with my partner.
\item When I'm not involved in a relationship, I feel somewhat anxious and insecure.
\item I feel comfortable depending on romantic partners.
\item I get frustrated when my partner is not around as much as I would like.
\item I don't mind asking romantic partners for comfort, advice, or help.
\item I get frustrated if romantic partners are not available when I need them.
\item It helps to turn to my romantic partner in times of need.
\item When romantic partners disapprove of me, I feel really bad about myself.
\item I turn to my partner for many things, including comfort and reassurance.
\item I resent it when my partner spends time away from me.
\end{enumerate}

Of these 36 items, items 3, 15, 19, 22, 25, 27, 29, 31, 33, and 35 are reverse scored. 
Even numbered items form the attachment-related anxiety scale, while odd numbered items form the attachment-related avoidance scale.
These subscales represent both the empirical results of prior factor analysis and a theoretical model describing adult attachment styles \cite{brennan1998self}.
 
\subsection{Analysis}
Analysis of the data will be conducted in three phases.

First, all items will be evaluated using classical test theory methods.
This will establish estimates of whole scale and subscale internal reliability, item difficulties, and item discrimination indices.

Next, exploratory factor analysis will be conducted using varimax rotations to establish orthogonal factors. 
Skree tests and the Kaiser criteria will be used to determine the number of factors to retain; if more than two factors are retained then models will be generated with both the two factor solution proposed by the scale originators and the solution identified using the screening criteria.

Finally, polytomous IRT models will be estimated to better evaluate the utility of each item and the overall information available to the test.
Multidimensional IRT models will be estimated for a two factor solution using unconstrained Graded Response Model \cite{samejima1997graded} and Generalized Partial Credit Models \cite{muraki1992generalized}.
The mirt \cite{chalmers2012mirt} package in R will be used to fit models using expectation-maximization algorithms \cite{dempster1977EM}.
Model fit statistics, including $M2$ will be compared to determine model selection.
Item information curves and total test information will be assessed.

\section{Results}
\subsection{Classical Test Theory}
Internal reliability for the total scale was $\alpha = 0.909$, with internal reliability for anxiety and avoidance at $\alpha = 0.912$ and $\alpha = 0.935$, respectively.
Item discrimination and difficulty are presented in Table  \ref{table:anxiety} and Table \ref{table:avoidance}.
All items showed moderate to high levels of discrimination for the total scale and their designated subscale, and all difficulties were within a moderate range. 
This suggests no major problems with any items.

\begin{table}
\centering
\begin{tabular}{rccc}
  \hline
 & Total Discrimination & Subscale Discrimination & Difficulty \\ 
  \hline
  Q2 & 0.37 & 0.61 & 0.47 \\ 
  Q4 & 0.34 & 0.67 & 0.51 \\ 
  Q6 & 0.34 & 0.54 & 0.48 \\ 
  Q8 & 0.41 & 0.55 & 0.54 \\ 
  Q10 & 0.37 & 0.56 & 0.53 \\ 
  Q12 & 0.46 & 0.57 & 0.70 \\ 
  Q14 & 0.42 & 0.50 & 0.53 \\ 
  Q16 & 0.48 & 0.50 & 0.70 \\ 
  Q18 & 0.45 & 0.54 & 0.51 \\ 
  Q20 & 0.47 & 0.61 & 0.63 \\ 
  Q22 & 0.34 & 0.67 & 0.53 \\ 
  Q24 & 0.41 & 0.54 & 0.57 \\ 
  Q26 & 0.38 & 0.55 & 0.67 \\ 
  Q28 & 0.40 & 0.56 & 0.67 \\ 
  Q30 & 0.55 & 0.57 & 0.55 \\ 
  Q32 & 0.45 & 0.50 & 0.54 \\ 
  Q34 & 0.39 & 0.50 & 0.47 \\ 
  Q36 & 0.39 & 0.54 & 0.69 \\
   \hline
\end{tabular}
\caption{Item Analysis for the Anxiety Subscale}
\label{table:anxiety}
\end{table}

\begin{table}
\centering
\begin{tabular}{rccc}
  \hline
 & Total Discrimination & Subscale Discrimination & Difficulty \\ 
  \hline
Q1 & 0.46 & 0.62 & 0.55 \\  
  Q3 & 0.54 & 0.67 & 0.47 \\ 
  Q5 & 0.42 & 0.73 & 0.55 \\ 
  Q7 & 0.52 & 0.69 & 0.50 \\ 
  Q9 & 0.48 & 0.69 & 0.53 \\ 
  Q11 & 0.30 & 0.55 & 0.56 \\ 
  Q13 & 0.45 & 0.66 & 0.53 \\ 
  Q15 & 0.49 & 0.57 & 0.50 \\ 
  Q17 & 0.45 & 0.60 & 0.52 \\ 
  Q19 & 0.42 & 0.62 & 0.54 \\ 
  Q21 & 0.40 & 0.67 & 0.65 \\ 
  Q23 & 0.59 & 0.73 & 0.47 \\ 
  Q25 & 0.56 & 0.69 & 0.55 \\ 
  Q27 & 0.51 & 0.69 & 0.48 \\ 
  Q29 & 0.48 & 0.55 & 0.61 \\ 
  Q31 & 0.48 & 0.66 & 0.47 \\ 
  Q33 & 0.54 & 0.57 & 0.44 \\ 
  Q35 & 0.61 & 0.60 & 0.46 \\ 
   \hline
   \hline
\end{tabular}
\caption{Item Analysis for the Avoidance Subscale}
\label{table:avoidance}
\end{table}

\subsection{Exploratory Factor Analysis}
To determine the number of factors to extract, a skree test was conducted (Figure \ref{fig:skree}).
Results suggest a five (Parallell Analysis) or six (Kaiser Criteria) factor solution.
Using the Very Simple Structure criteria \cite{revell1976vss}, four factors are suggested for extraction, though the thrid and fourth are marginal (Figure \ref{fig:vss}). 
For the purposes of thorough analysis, both a two factor and 5 factor solution will be extracted.

\begin{figure}
\centering
	\includegraphics[scale=1]{figure/skree.png} 
	\caption{Skree Plot and Non-Graphical Indices}
	\label{fig:skree}
\end{figure}

\begin{figure}
\centering
	\includegraphics[scale=1]{figure/vss.png} 
	\caption{Very Simple Structure Analysis}
	\label{fig:vss}
\end{figure}


\subsubsection{Five Factor Solution}
A five-factor exploratory factor analysis using varimax rotation and a polychoric correlation matrix successfully extracted five orthogonal factors (Table \ref{table:5FactorCorrelations}).
Factor loadings are presented in Tables \ref{table:5FactorLoadings1} and \ref{table:5FactorLoadings2}, with visualization in Figure \ref{fig:5FactorLoadingsFig}.
Model fit was satisfactory (RMSEA = 0.057, BIC = 22046.44, RMSR =  0.02).

\subsubsection{Two Factor Solution}
A two-factor exploratory factor analysis using varimax rotation and a polychoric correlation matrix successfully extracted two orthogonal factors (Table \ref{table:2FactorCorrelations}).
Factor loadings are presented in Tables \ref{table:2FactorLoadings1} and \ref{table:2FactorLoadings2}, with visualization in Figure  \ref{fig:2FactorLoadingsFig}.
Model fit was satisfactory (RMSEA = 0.092, BIC = 78169.03, RMSR =  0.06).
This model presents a simpler interpretation of the data, but is also a significantly poorer fit.

\begin{table}[ht]
\centering
\begin{tabular}{rrrrrr}
  \hline
 & Factor 1 & Factor 2 & Factor 3 & Factor 4 & Factor 5 \\ 
  \hline
Factor 1 & 1.00 & 0.02 & 0.09 & -0.00 & -0.01 \\ 
  Factor 2 & 0.02 & 1.00 & -0.00 & -0.09 & 0.09 \\ 
 Factor 3 & 0.09 & -0.00 & 1.00 & 0.05 & -0.00 \\ 
  Factor 4 & -0.00 & -0.09 & 0.05 & 1.00 & -0.07 \\
 Factor 5 & -0.01 & 0.09 & -0.00 & -0.07 & 1.00 \\ 
   \hline
\end{tabular}
\caption{5-Factor Solution Correlations}
\label{table:5FactorCorrelations}
\end{table}

\begin{table}[ht]
\centering
\begin{tabular}{rrrrrr}
  \hline
Item & Factor 1 & Factor 2 & Factor 3 & Factor 4 & Factor 5 \\
  \hline
  Q2 & 0.08 & \textbf{0.81} & -0.00 & 0.12 & 0.08 \\ 
  Q4 & 0.11 & \textbf{0.67} & -0.01 & 0.15 & 0.18 \\ 
  Q6 & 0.12 & \textbf{0.67} & 0.04 & 0.13 & 0.33 \\ 
  Q8 & 0.06 & \textbf{0.76} & -0.03 & 0.14 & 0.18 \\ 
  Q10 & 0.05 & \textbf{0.55} & 0.06 & 0.18 & 0.46 \\ 
  Q12 & -0.07 & 0.32 & -0.11 & 0.18 & \textbf{0.66} \\ 
  Q14 & 0.01 & \textbf{0.69} & -0.08 & 0.18 & 0.12 \\ 
  Q16 & -0.08 & 0.34 & -0.15 & 0.15 & \textbf{0.66} \\ 
  Q18 & 0.05 & \textbf{0.56} & -0.10 & 0.38 & 0.23 \\ 
  Q20 & -0.04 & 0.32 & -0.12 & 0.39 & \textbf{0.45} \\ 
  Q22 & -0.05 & \textbf{0.72} & 0.08 & 0.06 & 0.03 \\ 
  Q24 & 0.02 & 0.33 & -0.07 & \textbf{0.57} & 0.26 \\ 
  Q26 & -0.00 & 0.34 & 0.04 & 0.23 & \textbf{0.63} \\ 
  Q28 & -0.04 & \textbf{0.45} & -0.12 & 0.29 & 0.16 \\ 
  Q30 & -0.14 & 0.36 & -0.16 & \textbf{0.66} & 0.14 \\ 
  Q32 & -0.03 & 0.26 & -0.19 & \textbf{0.70} & 0.10 \\ 
  Q34 & 0.01 & \textbf{0.50} & -0.16 & 0.29 & 0.09 \\ 
  Q36 & -0.01 & 0.33 & -0.01 & \textbf{0.59} & 0.21 \\
   \hline
\end{tabular}
\caption{5-Factor Solution Loadings, Anxiety Items}
\label{table:5FactorLoadings1}
\end{table}

\begin{table}[ht]
\centering
\begin{tabular}{rrrrrr}
  \hline
Item & Factor 1 & Factor 2 & Factor 3 & Factor 4 & Factor 5 \\
  \hline
  Q1 & \textbf{0.53} & 0.01 & 0.41 & 0.04 & -0.10 \\ 
  Q3 & \textbf{0.60} & -0.03 & 0.39 & -0.07 & -0.12 \\ 
  Q5 & \textbf{0.78} & 0.09 & 0.16 & -0.01 & -0.00 \\ 
  Q7 & \textbf{0.82} & 0.00 & 0.17 & -0.04 & -0.08 \\ 
  Q9 & \textbf{0.66} & 0.09 & 0.41 & 0.04 & -0.06 \\ 
  Q11 & \textbf{0.72} & 0.21 & 0.15 & 0.01 & 0.08 \\ 
  Q13 & \textbf{0.83} & 0.08 & 0.14 & -0.03 & 0.00 \\ 
  Q15 & 0.39 & 0.02 & \textbf{0.62} & 0.01 & -0.15 \\ 
  Q17 & \textbf{0.75} & 0.05 & 0.25 & -0.00 & 0.06 \\ 
  Q19 & \textbf{0.48} & 0.10 & \textbf{0.47} & -0.07 & -0.02 \\ 
  Q21 & \textbf{0.58} & -0.02 & 0.23 & -0.03 & 0.02 \\ 
  Q23 & \textbf{0.73} & -0.17 & 0.27 & -0.02 & -0.03 \\ 
  Q25 & 0.37 & -0.04 & \textbf{0.69} & -0.09 & -0.12 \\ 
  Q27 & 0.34 & 0.00 & \textbf{0.76} & -0.06 & -0.03 \\ 
  Q29 & \textbf{0.42} & -0.05 & \textbf{0.45} & -0.15 & -0.04 \\ 
  Q31 & 0.36 & -0.00 & \textbf{0.70} & -0.07 & -0.00 \\ 
  Q33 & 0.34 & -0.12 & \textbf{0.67} & -0.18 & 0.03 \\ 
  Q35 & 0.32 & -0.24 & \textbf{0.66} & -0.23 & 0.01 \\ 
   \hline
\end{tabular}
\caption{5-Factor Solution Loadings, Avoidance Items}
\label{table:5FactorLoadings2}
\end{table}

\begin{table}[ht]
\centering
\begin{tabular}{rrr}
  \hline
 & Factor 1 & Factor 2 \\ 
  \hline
Factor 1 & 1.00 & 0.00 \\ 
  Factor 2 & 0.00 & 1.00 \\ 
   \hline
\end{tabular}
\caption{2-Factor Solution Correlations}
\label{table:2FactorCorrelations}
\end{table}

\begin{table}[ht]
\centering
\begin{tabular}{rrr}
  \hline
 & Factor 1 & Factor 2\\ 
  \hline
  Q2 & 0.10 & \textbf{0.72} \\ 
  Q4 & 0.10 & \textbf{0.68} \\ 
  Q6 & 0.14 & \textbf{0.72} \\ 
  Q8 & 0.07 & \textbf{0.74} \\ 
  Q10 & 0.08 & \textbf{0.69} \\ 
  Q12 & -0.14 & \textbf{0.60} \\ 
  Q14 & -0.01 & \textbf{0.68} \\ 
  Q16 & -0.16 & \textbf{0.60} \\ 
  Q18 & -0.02 & \textbf{0.73} \\ 
  Q20 & -0.13 & \textbf{0.62} \\ 
  Q22 & 0.05 & \textbf{0.58} \\ 
  Q24 & -0.05 & \textbf{0.63} \\ 
  Q26 & 0.00 & \textbf{0.61} \\ 
  Q28 & -0.10 & \textbf{0.56} \\ 
  Q30 & -0.24 & \textbf{0.63} \\ 
  Q32 & -0.17 & \textbf{0.56} \\ 
  Q34 & -0.08 & \textbf{0.58} \\ 
  Q36 & -0.05 & \textbf{0.60} \\ 
   \hline
\end{tabular}
\caption{2-Factor Solution Loadings, Anxiety Items}
\label{table:2FactorLoadings1}
\end{table}

\begin{table}[ht]
\centering
\begin{tabular}{rrr}
  \hline
 & Factor 1 & Factor 2 \\ 
  \hline
Q1 & \textbf{0.67} & -0.02 \\ 
  Q3 & \textbf{0.72} & -0.11 \\ 
  Q5 & \textbf{0.72} & 0.11 \\ 
  Q7 & \textbf{0.75} & -0.00 \\ 
  Q9 & \textbf{0.78} & 0.07 \\ 
  Q11 & \textbf{0.68} & 0.24 \\ 
  Q13 & \textbf{0.75} & 0.10 \\ 
  Q15 & \textbf{0.69} & -0.08 \\ 
  Q17 & \textbf{0.75} & 0.09 \\ 
  Q19 & \textbf{0.68} & 0.02 \\ 
  Q21 & \textbf{0.60} & 0.00 \\ 
  Q23 & \textbf{0.73} & -0.12 \\ 
  Q25 & \textbf{0.71} & -0.17 \\ 
  Q27 & \textbf{0.71} & -0.09 \\ 
  Q29 & \textbf{0.61} & -0.14 \\ 
  Q31 & \textbf{0.70} & -0.09 \\ 
  Q33 & \textbf{0.67} & -0.21 \\ 
  Q35 & \textbf{0.64} & -0.33 \\ 
   \hline
\end{tabular}
\caption{2-Factor Solution Loadings, Avoidance Items}
\label{table:2FactorLoadings2}
\end{table}

\begin{figure}
	\centering
	\includegraphics[scale=1]{figure/5FactorSolution.png} 
	\caption{5 Factor Solution. All associations greater than $r \pm 0.05$ are represented. Items have been re-ordered for visual clarity.}
	\label{fig:5FactorLoadingsFig}
\end{figure}

\begin{figure}
\centering
	\includegraphics[scale=1]{figure/2FactorSolution.png} 
	\caption{2 Factor Solution. All associations greater than $r \pm 0.05$ are represented. Items have been re-ordered for visual clarity.}
	\label{fig:2FactorLoadingsFig}
\end{figure}

\subsection{Item Response Theory}
Both GRM and GPCM models reached convergence. 
Both models provided acceptable global fit (Table \ref{table:fit}), with overall fit favoring the GRM.
Coefficients for the GRM model are presented in Tables \ref{table:coef1} and \ref{table:coef2}.
3-dimensional test information and standard error curves are presented in Figures \ref{fig:info} and \ref{fig:error}.

\begin{figure}
	\centering
	\includegraphics[scale=.8]{figure/info.png} 
	\caption{Test Information Curve}
	\label{fig:info}
\end{figure}

\begin{figure}
	\centering
	\includegraphics[scale=.8]{figure/error.png} 
	\caption{Test Error Curve}
	\label{fig:error}
\end{figure}


\begin{table}
\centering
\begin{tabular}{lrrrr}\hline
Model & GRM & GPCM & Diff & 5-Factor \\ \hline
AIC & 1628170 & 1651312 & -23142 & 1585031 \\
BIC & 1630119 & 1653261 & -23142 & 1587748\\
-2 Log Likelihood & 1627668 & 1650810 & -23142 & 1584331 \\
M2 &  27752.26 & 34445.73 &  -6693.47 & 7461.311\\
df & 415 & 415 & 0 &  316 \\
RMSEA & 0.061 & 0.068 & -0.007 & 0.036\\
TLI & 0.939 & 0.924 &  0.015 & 0.979\\
CFI & 0.947 & 0.935 &  0.012 & 0.986 \\
SRMSR & 0.056 & 0.053 &  0.003 & 0.0328\\ \hline
\end{tabular}
\caption{Fit Indices for IRT Models. GRM and GPCM represent 2-dimensional IRT models. Diff represents the difference in fit indices between the two dimensional models. 5-Factor represents fit indices for an exploratory 5-dimensional graded response model.}
\label{table:fit}
\end{table}

\begin{table}[ht]
\centering
\begin{tabular}{rrrrrrrr}
  \hline
 & a1 & a2 & d1 & d2 & d3 & d4 & d5 \\ 
  \hline
  Q2 & 2.04 & -0.43 & 7.90 & 3.83 & 1.97 & 1.01 & -1.42 \\ 
  Q4 & 1.80 & -0.41 & 6.49 & 3.78 & 1.69 & 0.41 & -1.93 \\ 
  Q6 & 2.03 & -0.53 & 8.23 & 3.73 & 1.74 & 0.86 & -1.50 \\ 
  Q8 & 2.19 & -0.38 & 8.35 & 3.67 & 1.42 & 0.10 & -2.47 \\ 
  Q10 & 1.81 & -0.34 & 7.70 & 3.15 & 1.35 & -0.04 & -1.83 \\ 
  Q12 & 1.38 & 0.21 & 7.00 & 1.40 & -0.35 & -1.51 & -3.11 \\ 
  Q14 & 1.74 & -0.12 & 6.62 & 2.60 & 1.11 & 0.32 & -1.50 \\ 
  Q16 & 1.43 & 0.26 & 7.00 & 1.53 & -0.41 & -1.52 & -3.35 \\ 
  Q18 & 2.00 & -0.11 & 7.20 & 3.53 & 1.61 & 0.44 & -1.78 \\ 
  Q20 & 1.47 & 0.18 & 6.76 & 2.06 & 0.23 & -0.70 & -2.89 \\ 
  Q22 & 1.67 & -0.28 & 3.08 & 1.11 & 0.18 & -1.88 & -6.42 \\ 
  Q24 & 1.47 & 0.00 & 6.80 & 2.64 & 0.89 & -0.16 & -2.46 \\ 
  Q26 & 1.40 & -0.14 & 6.41 & 2.11 & -0.02 & -1.34 & -3.39 \\ 
  Q28 & 1.24 & 0.13 & 6.41 & 1.35 & -0.03 & -0.88 & -2.47 \\ 
  Q30 & 1.60 & 0.45 & 6.95 & 3.10 & 1.13 & -0.00 & -2.54 \\ 
  Q32 & 1.29 & 0.27 & 6.54 & 3.02 & 1.14 & 0.11 & -2.25 \\ 
  Q34 & 1.33 & 0.07 & 6.37 & 3.26 & 1.71 & 0.74 & -1.10 \\ 
  Q36 & 1.35 & 0.00 & 6.56 & 1.75 & -0.19 & -1.41 & -3.12 \\ 
   \hline
\end{tabular}
\caption{GRM Item Coefficients, Anxiety Items}
\label{table:coef1}
\end{table}

\begin{table}[ht]
\centering
\begin{tabular}{rrrrrrrr}
  \hline
 & a1 & a2 & d1 & d2 & d3 & d4 & d5 \\ 
  \hline
  Q1 & -0.21 & -1.70 & 7.34 & 2.10 & -0.10 & -0.90 & -3.30 \\ 
  Q3 & -0.40 & -2.00 & 1.42 & -0.88 & -2.07 & -4.28 & -7.63 \\ 
  Q5 & 0.14 & -2.02 & 7.53 & 2.26 & -0.01 & -1.12 & -3.30 \\ 
  Q7 & -0.22 & -2.20 & 8.28 & 1.80 & -0.55 & -1.68 & -3.91 \\ 
  Q9 & 0.00 & -2.48 & 7.95 & 2.59 & -0.28 & -1.55 & -4.16 \\ 
  Q11 & 0.48 & -1.94 & 7.56 & 2.27 & 0.06 & -1.05 & -3.29 \\ 
  Q13 & 0.07 & -2.28 & 8.13 & 2.31 & -0.35 & -1.43 & -4.09 \\ 
  Q15 & -0.31 & -1.81 & 1.80 & -0.64 & -1.66 & -3.55 & -7.09 \\ 
  Q17 & 0.03 & -2.24 & 7.69 & 2.37 & -0.36 & -1.74 & -4.43 \\ 
  Q19 & -0.05 & -1.78 & 2.65 & -0.12 & -1.52 & -3.69 & -6.35 \\ 
  Q21 & -0.16 & -1.41 & 6.34 & 2.88 & 0.89 & -0.05 & -2.02 \\ 
  Q23 & -0.63 & -2.15 & 6.98 & 1.89 & -0.99 & -2.41 & -4.90 \\ 
  Q25 & -0.59 & -1.81 & 2.25 & -0.08 & -1.13 & -3.15 & -7.24 \\ 
  Q27 & -0.38 & -1.84 & 2.00 & -0.93 & -2.09 & -4.15 & -7.02 \\ 
  Q29 & -0.41 & -1.42 & 2.90 & 0.54 & -0.65 & -2.56 & -6.57 \\ 
  Q31 & -0.35 & -1.78 & 1.76 & -1.06 & -2.08 & -4.01 & -7.20 \\ 
  Q33 & -0.67 & -1.66 & 1.70 & -1.36 & -2.83 & -4.41 & -6.82 \\ 
  Q35 & -1.02 & -1.59 & 1.76 & -1.16 & -2.52 & -4.41 & -6.92 \\ 
   \hline
\end{tabular}
\caption{GRM Item Coefficients, Avoidance Items}
\label{table:coef2}
\end{table}

\section{Discussion}
\subsection{Item Analysis}
As expected, internal reliability was high for both the total scale and all subscales. 
All item discrimination parameters were well matched to the theoretical 2-factor structure of the instrument.
These values suggest the instrument is well-balanced for discriminating between individuals with high and low levels of attachment-related anxiety and attachment-related avoidance.

\subsection{Factor Analysis}
Although the five-factor solution provided a marginally better fit to the overall data, the two-factor solution provides a more readily interpreted solution.
Interestingly, the additional factors identified in the five-factor solution manifest as subsets of the two larger identified factors.
As a result, it may be practical to expand scoring to reflect these additional subscales and enhance the clinical utility of the instrument.

\subsection{IRT Models}
The 2-factor GRM provides additional useful information regarding the utility of the ECR.
Like most instruments, the ECR provides the greatest amount of information when participants have more normal response patterns, and provide less information about extreme cases, particularly when participants have extremely low or extremely high scores on both factors.
Although a broader range of avoidance can be assessed through the ECR, information about anxiety is generally richer and more useful.
By using a multidimensional scoring model, individual attachment characteristics can be more accurately scored measured, improving the overall utility of the instrument.

\subsection{Summary}
These analyses support the continued use of the ECR as a clinical and research instrument, and provide further evidence for reliability and factor structure. 
New parameter estimates allow the use of all scale data to estimate scores on each factor, allowing for more accurate estimation of individual traits.

\subsection{Limitations \& Future Research}
Despite the use of a large sample and robust analysis techniques, the preceding results should be approached with caution.
The data used to develop this model represents a large convenience sample, with few safeguards against duplication and facetious data entry. 
Although the item analysis, data structure, and IRT findings resemble those of previous studies \cite{brennan1998self, fraley2000item}, they may not be truly representative.
The models in this study are exploratory; confirmation with another data set is necessary.
Additionally, current ECR scoring is not based in IRT ability metrics, so practical scaling must be conducted to allow for practical usage.
Although the current estimates may provide more precise measures of ability than past models through the use of multidimensional scoring, the degree to which this model improves upon raw score-based and single-factor IRT estimate classifications remains examined. 
It is possible the marginal increase in measurement accuracy may be insufficient to warrant the additional computational resources.
Finally, because this data is based on the original ECR rather than more current instruments, comparison with currently used instruments may not be appropriate.
Future research should attempt confirmatory analyses, and continue to explore the utility of the five-factor model in clinical applications.
Scaling and concordance studies can be conducted to determine whether the multidimensional scoring model provides a useful improvement over existing scoring strategies. 

\pagebreak
\section{Supplement}
As an additional exploratory analysis, a five factor multidimensional graded response model was fit to the data using quasi-Monte Carlo EM estimation (QMCEM).
The model achieved a tolerance of 0.0005 after 500 iterations.
Model fit indices are provided in Table \ref{table:fit}.
Model parameters are provided in Tables \ref{table:5dparam1} and \ref{table:5dparam2}.
As expected, the five dimensional model provides a significantly improved fit to the data, but also results in a solution that is considerably more difficult to interpret. 
Due to the complex multidimensional structure of the model, visualization of item and test information curves is not easily achieved; instead it is necessary to rely on parameter estimates to assess information density and breadth.
In practice this model is likely overly complex for the task at hand, as it provides more nuanced estimates of individual traits than is likely to be clinically relevant.
However, this model may be useful for detecting small treatment effects in large samples in research applications.

\begin{table}[ht]
\centering
\begin{tabular}{rrrrrrrrrrr}
  \hline
 & a1 & a2 & a3 & a4 & a5 & d1 & d2 & d3 & d4 & d5 \\ 
  \hline
  Q2 & 1.92 & 0.18 & -2.36 & -1.12 & 0.24 & 9.18 & 4.78 & 2.56 & 1.36 & -1.71 \\ 
  Q4 & 1.52 & 0.03 & -1.52 & -0.52 & 0.38 & 6.60 & 3.93 & 1.79 & 0.46 & -1.98 \\ 
  Q6 & 1.69 & -0.18 & -1.58 & -0.56 & 0.83 & 8.27 & 3.88 & 1.85 & 0.94 & -1.53 \\ 
  Q8 & 1.89 & 0.17 & -1.95 & -0.87 & 0.52 & 8.76 & 4.07 & 1.64 & 0.17 & -2.70 \\ 
  Q10 & 1.66 & -0.26 & -1.02 & -0.44 & 1.14 & 7.89 & 3.37 & 1.45 & -0.04 & -1.93 \\ 
  Q12 & 1.72 & 0.25 & -0.34 & 0.15 & 2.04 & 8.73 & 1.83 & -0.49 & -2.03 & -4.07 \\ 
  Q14 & 1.52 & 0.35 & -1.39 & -0.63 & 0.28 & 6.79 & 2.79 & 1.23 & 0.39 & -1.57 \\ 
  Q16 & 1.73 & 0.38 & -0.44 & 0.16 & 2.16 & 8.88 & 2.02 & -0.59 & -2.08 & -4.43 \\ 
  Q18 & 2.04 & 0.31 & -0.98 & -0.19 & 0.29 & 7.13 & 3.52 & 1.61 & 0.45 & -1.78 \\ 
  Q20 & 1.79 & 0.27 & -0.19 & 0.13 & 0.86 & 7.18 & 2.20 & 0.21 & -0.80 & -3.12 \\ 
  Q22 & 1.46 & 0.13 & -1.68 & -1.05 & 0.21 & 3.62 & 1.40 & 0.29 & -2.14 & -7.18 \\ 
  Q24 & 2.14 & 0.15 & -0.18 & 0.16 & 0.12 & 7.42 & 2.93 & 0.98 & -0.21 & -2.80 \\ 
  Q26 & 1.67 & -0.30 & -0.38 & -0.01 & 1.60 & 7.38 & 2.56 & -0.06 & -1.67 & -4.08 \\ 
  Q28 & 1.25 & 0.35 & -0.50 & -0.18 & 0.28 & 6.34 & 1.35 & -0.03 & -0.88 & -2.46 \\ 
  Q30 & 2.80 & 0.76 & 0.11 & -0.05 & -0.19 & 8.41 & 3.89 & 1.41 & -0.03 & -3.26 \\ 
  Q32 & 2.65 & 0.66 & 0.14 & 0.42 & -0.46 & 8.44 & 4.00 & 1.50 & 0.09 & -3.12 \\ 
  Q34 & 1.34 & 0.47 & -0.77 & 0.00 & 0.00 & 6.45 & 3.28 & 1.72 & 0.75 & -1.12 \\ 
  Q36 & 2.15 & 0.00 & 0.00 & 0.00 & 0.00 & 7.20 & 2.01 & -0.24 & -1.66 & -3.65 \\ 
   \hline
\end{tabular}
\caption{Five-Dimensional GRM, Anxiety Items}
\label{table:5dparam1}
\end{table}

\begin{table}[ht]
\centering
\begin{tabular}{rrrrrrrrrrr}
  \hline
 & a1 & a2 & a3 & a4 & a5 & d1 & d2 & d3 & d4 & d5 \\ 
  \hline
  Q1 & -0.09 & -1.76 & -0.68 & 0.39 & -0.61 & 7.40 & 2.13 & -0.09 & -0.91 & -3.33 \\ 
  Q3 & -0.35 & -1.87 & -0.98 & 0.86 & -0.62 & 1.42 & -0.90 & -2.10 & -4.33 & -7.68 \\ 
  Q5 & 0.08 & -1.68 & -1.76 & 1.59 & -0.43 & 8.41 & 2.57 & -0.05 & -1.34 & -3.84 \\ 
  Q7 & -0.30 & -2.00 & -1.83 & 1.96 & -0.75 & 9.60 & 2.16 & -0.71 & -2.12 & -4.83 \\ 
  Q9 & 0.07 & -2.31 & -1.31 & 0.74 & -0.65 & 7.84 & 2.57 & -0.28 & -1.54 & -4.14 \\ 
  Q11 & 0.38 & -1.55 & -1.74 & 1.25 & -0.13 & 8.10 & 2.44 & 0.02 & -1.18 & -3.58 \\ 
  Q13 & -0.00 & -2.04 & -2.13 & 2.02 & -0.48 & 9.74 & 2.81 & -0.49 & -1.84 & -5.09 \\ 
  Q15 & -0.16 & -2.42 & -0.51 & -0.08 & -0.69 & 2.03 & -0.69 & -1.83 & -3.92 & -7.69 \\ 
  Q17 & 0.03 & -1.99 & -1.47 & 1.37 & -0.26 & 8.00 & 2.47 & -0.44 & -1.89 & -4.68 \\ 
  Q19 & -0.08 & -1.83 & -0.90 & 0.33 & -0.24 & 2.67 & -0.11 & -1.51 & -3.67 & -6.32 \\ 
  Q21 & -0.16 & -1.26 & -0.77 & 0.70 & -0.24 & 6.33 & 2.87 & 0.88 & -0.07 & -2.04 \\ 
  Q23 & -0.50 & -2.10 & -0.92 & 1.52 & -0.56 & 7.33 & 1.96 & -1.11 & -2.61 & -5.20 \\ 
  Q25 & -0.57 & -2.79 & -0.34 & -0.28 & -0.61 & 2.71 & -0.07 & -1.31 & -3.68 & -8.41 \\ 
  Q27 & -0.34 & -3.25 & -0.35 & -0.43 & -0.29 & 2.63 & -1.18 & -2.67 & -5.30 & -8.75 \\ 
  Q29 & -0.47 & -1.56 & -0.53 & 0.22 & -0.23 & 2.93 & 0.55 & -0.64 & -2.56 & -6.57 \\ 
  Q31 & -0.33 & -2.74 & -0.40 & -0.22 & -0.19 & 2.09 & -1.23 & -2.43 & -4.65 & -8.14 \\ 
  Q33 & -0.78 & -2.66 & -0.20 & -0.12 & 0.00 & 2.02 & -1.56 & -3.27 & -5.09 & -7.81 \\ 
  Q35 & -1.17 & -2.70 & 0.00 & 0.00 & 0.00 & 2.10 & -1.35 & -2.94 & -5.14 & -8.05 \\ 
   \hline
\end{tabular}
\caption{Five-Dimensional GRM, Avoidance Items}
\label{table:5dparam2}
\end{table}




\bibliography{journalrefs}

\end{document}
