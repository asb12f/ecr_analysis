# Project Description
This project contains a psychometric analysis and write up for the Experiences in Close Relationships Scale.
Data from 17,386 participants will be analyzed using CTT, exploratory factor analysis, and IRT.

# Contributors
## Analysis and Report
Andrew Benesh, MS
## Original Data Set
[Personality Testing](http://personality-testing.info/_rawdata/)

