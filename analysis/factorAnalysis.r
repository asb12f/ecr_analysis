# these lines orient R to the correct directories
oldwd <- getwd()
setwd("C:/Program Files/Ampps/www/repos/ecr_analysis/")
library(psych)
# reading the data into R

data <- read.table("./data/data_clean.txt", sep="\t")

# doing a little cleaning for clarity

for (i in 1:36) {
data[data==-1] <- NA
names(data)[i] <- paste("Q", i, sep="")
}

#fixing reverse coding
data[c(3, 15, 19, 22, 25, 27, 29, 31, 33, 35)] <-  6 - data[c(3, 15, 19, 22, 25, 27, 29, 31, 33, 35)]

names(data)[37] <- "age"
names(data)[38] <- "sex"
data$sex <- factor(data$sex,
                    levels = c(1,2,3, -1),
                    labels = c("male", "female", "other", "missed"))
names(data)[39] <- "country"

items <- data[1:36]
items <- data.frame(items)
anxiety <- data[c(2,4,6,8,10,12,14,16,18,20,22,24,26,28,30,32,34,36)]
anxiety <- data.frame(anxiety)
avoidance <- data[c(1,3,5,7,9,11,13,15,17,19,21,23,25,27,29,31,33,35)]
avoidance <- data.frame(avoidance)
# Analysis Starts Here
summary(items)
# Basic psychometric evaluation
reliability <- alpha(items, key=NULL)
reliability_anxiety <- alpha(anxiety, key=NULL)
reliability_avoidance <- alpha(avoidance, key=NULL)

cronbach <- reliability$total[1]
cronbach_anxiety <- reliability_anxiety$total[1]
cronbach_avoidance <- reliability_avoidance$total[1]

ITC <- reliability$item.stats[5]
names(ITC)[1] <- "Item Total Correlation"
ITC_anxiety <- reliability_anxiety$item.stats[5]
names(ITC)[1] <- "Item Total Correlation"
ITC_avoidance <- reliability_avoidance$item.stats[5]
names(ITC)[1] <- "Item Total Correlation"
diff <- reliability$item.stats[6] / 5
names(diff)[1] <- "Item Difficulty"

#Builds a nice data frame of difficlty and discrimination indices
library(xtable)
item_Analysis_anxiety <- data.frame(round(ITC,2), round(ITC_anxiety, 2), round(diff,2))
item_Analysis_avoidance <- data.frame(round(ITC,2), round(ITC_avoidance, 2), round(diff,2))
xtable(item_Analysis_anxiety)
xtable(item_Analysis_avoidance)

# Now let's run an exploratory factor analysis on the items
## First, we'll use the nFactors package to determine how many factors to extract

library(nFactors)
ev <- eigen(cor(items))
ap <- parallel(subject=nrow(items),var=ncol(items),
               rep=100,cent=.05)
nS <- nScree(x=ev$values, aparallel=ap$eigen$qevpea)
plotnScree(nS)

vss <- vss(items[1:36],title="Very Simple Structure")


## Based on this analysis, we will extract 5 factors
fit <- fa.poly(items,nfactors=5, rotate="varimax", 
          fm="pa",alpha=.05, scores="regression", 
          oblique.scores=TRUE, residuals=TRUE, cor="poly")
fit
xtable(fit$fa$loadings[1:36, 1:5])
xtable(fit$scores$r.scores)


fa.diagram(fit,Phi=NULL,fe.results=NULL,sort=TRUE,cut=.5,
 simple=FALSE, errors=FALSE,g=FALSE,digits=2,e.size=.05,rsize=.2,side=2,
adj=1, main="5 Factor Solution")

fit2 <- fa.poly(items,nfactors=2, rotate="varimax", 
               fm="pa",alpha=.05, scores="regression", 
               oblique.scores=TRUE, residuals=TRUE, cor="poly")
fit2

fa.diagram(fit2,Phi=NULL,fe.results=NULL,sort=TRUE,cut=.5,
           simple=FALSE, errors=FALSE,digits=2,e.size=.05, side=2,adj=1.4, main="2 Factor Solution")


xtable(fit2$fa$loadings[1:36, 1:2])
xtable(fit2$scores$r.scores)

# Finally, let's fit some IRT models to the data
## because our data are polytomous and multidimensional, 
## we'll examine the Graded Response Model
## and the Generalized Partial Credit Model.
library(mirt)

# We'll fit two models to see which provides the best fit for our data.

GRM2 <- mirt(items, 2, itemtype='graded')
GPCM2 <- mirt(items, 2, itemtype='gpcm')

## Modelled purely because of curiosity
GRM5 <- mirt(items, 5, itemtype='graded', method='QMCEM')

M2(GRM2)
M2(GPCM2)
M2(GRM5)

xtable((coef(GRM2, CI = 0.95, printSE = FALSE,
     rotate = "varimax", Target = NULL, digits = 3, IRTpars = FALSE,
     rawug = FALSE, as.data.frame = FALSE, simplify = TRUE,
     verbose = TRUE))$items)

xtable((coef(GRM5, CI = 0.95, printSE = FALSE,
             rotate = "varimax", Target = NULL, digits = 3, IRTpars = FALSE,
             rawug = FALSE, as.data.frame = FALSE, simplify = TRUE,
             verbose = TRUE))$items)

plot(GRM2, type="info")
plot(GRM2, type="SE")

plot(GRM5, type="info")   
plot(GRM5, type="SE")

Theta <- matrix(seq(-4,4,.01))
Theta2 <- as.matrix(expand.grid(Theta, Theta))
tinfo <- testinfo(GRM2, Theta2, degrees=c(45,45))

# this line just helps clean things up
setwd(oldwd)